<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Business;
use App\Models\Store;
use App\Domains\Auth\Models\User;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use App\Http\Controllers\ParrotController;

class ParrotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    
        

        // Numero de Negocios que se crearan.
        for ($i = 0; $i < 4; $i++) {

            // En caso de estar en desarrollo / testing
            if (app()->environment(['local', 'testing'])) {


                // Se crea el objeto Faker
                $faker = \Faker\Factory::create('es_ES');
                //$faker->addProvider(new \FakerRestaurant\Provider\es_PE\Restaurant($faker));

                $parrot=new ParrotController();


                // Se crea un negocio
                $business_information=array(
                        'name' => $faker->company(),
                        'logo' => $faker->imageUrl($width = 640, $height = 480,'abstract'),
                        'address' => $faker->address(),
                        'city' => $faker->city(),
                        'state' => $faker->state(),
                        'country' => $faker->country(),
                        'zip' => $faker->postcode(),
                        'phone' => $faker->phoneNumber(),
                        'latitude' => $faker->latitude(),
                        'longitude' => $faker->longitude()

                    );
                $business=$parrot->createBusiness($business_information);

                

                
                


                // Se crea un usuario asignado al negocio, el cual contendra 1 o mas tiendas

                $user=User::create([
                    'type' => User::TYPE_USER,
                    'name' => 'Test User',
                    // 'email' => 'user'.($i+rand(1000,10000)).'@user.com',
                    'email' => 'user'.$i.'@user.com',
                    'password' => 'secret',
                    'email_verified_at' => now(),
                    'active' => true,
                ]);

                
                // Se comienzan a crear las tiendas
                for ($store_num = 0; $store_num < rand(1, 2); $store_num++) {
                    $faker_store= \Faker\Factory::create('es_ES');

                    $store_information=array(
                        'name' => 'Store '.$store_num,
                        'photo' => $faker_store->imageUrl($width = 640, $height = 480,'city'),
                        'address' => $faker_store->address(),
                        'city' => $faker_store->city(),
                        'state' => $faker_store->state(),
                        'country' => $faker_store->country(),
                        'zip' => $faker_store->postcode(),
                        'phone' => $faker_store->phoneNumber(),
                        'latitude' => $faker_store->latitude(),
                        'longitude' => $faker_store->longitude(),
                    );

                    $store=$parrot->createStore($store_information);

                    // Se crea la relacion tienda negocio y tienda usuario
                    $store->businesses()->attach($business->id);
                    $store->users()->attach($user->id);
                    

                    // Comenzamo el llenado de ordenes de un periodo de 10 dias con un llenado aleatorio por hora
                    $month_ago = Carbon::today()->startOfDay()->subDays(10);
                    $period = CarbonPeriod::create($month_ago, Carbon::today());

                    foreach ($period as $historical_date) {
                        echo "Generando ordenes del dia:".$historical_date."\r\n";
                        for($happy_hour=8;$happy_hour < 24; $happy_hour++){

                            $invoice_date= $historical_date->copy()->addHours($happy_hour)."\r\n";
                            for ($invoice_num = 0; $invoice_num < rand(1, 5); $invoice_num++) {
                                $faker_client= \Faker\Factory::create('es_ES');
                                $client=$parrot->clientExist($faker_client->company(),$business->id);

                                $total=0;
                                $products=array();
                                for ($products_num=0; $products_num < rand(2,20);$products_num++){
                                    $faker_product= \Faker\Factory::create('es_ES');
                                    $faker->addProvider(new \FakerRestaurant\Provider\es_PE\Restaurant($faker));
                                    $type=rand(1,2);
                                    if ($type==1){
                                        $price=$faker->randomFloat(2,5,280);
                                        $total=$total+$price;
                                        $product=array('name'=>$faker->foodName(),'price'=>$price);
                                    }else{
                                        $price=$faker->randomFloat(2,5,100);
                                        $total=$total+$price;
                                        $product=array('name'=>$faker->beverageName(),'price'=>$price);    
                                    }
                                    array_push($products,$product);
                                }
                                $invoice_number=$parrot->createInvoice($store->id,$client,$user->id,$total,$invoice_date);
                                $parrot->addProducts($products,array("store_id"=>$store->id,"business_id"=>$business->id),$invoice_number->id,$invoice_date);

                            }
                        }
                        
                    }
                    
                    



                }
         
            }
            


        }


    }
}
