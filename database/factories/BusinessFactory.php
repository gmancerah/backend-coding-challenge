<?php

namespace Database\Factories;

use App\Models\Business;
use Illuminate\Database\Eloquent\Factories\Factory;

class BusinessFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Business::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $faker = \Faker\Factory::create('es_ES');
        $faker->addProvider(new \FakerRestaurant\Provider\es_PE\Restaurant($faker));
        return [
            'name' => $faker->company(),
            'logo' => $faker->imageUrl($width = 640, $height = 480,'abstract'),
            'address' => $faker->address(),
            'city' => $faker->city(),
            'state' => $faker->state(),
            'country' => $faker->country(),
            'zip' => $faker->postcode(),
            'phone' => $faker->phoneNumber(),
            'latitude' => $faker->latitude(),
            'longitude' => $faker->longitude()
        ];
    }
}
