# Backend Coding Challenge
## Desarrollo de API-REST

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)
[![Latest Stable Version](https://poser.pugx.org/rappasoft/laravel-boilerplate/v/stable)](https://packagist.org/packages/rappasoft/laravel-boilerplate)
[![Latest Unstable Version](https://poser.pugx.org/rappasoft/laravel-boilerplate/v/unstable)](https://packagist.org/packages/rappasoft/laravel-boilerplate) 
![Tests](https://github.com/rappasoft/laravel-boilerplate/workflows/Tests/badge.svg?branch=master)
![GitHub contributors](https://img.shields.io/github/contributors/rappasoft/laravel-boilerplate.svg)
![GitHub stars](https://img.shields.io/github/stars/rappasoft/laravel-boilerplate.svg?style=social)
 ------------------------ Basado en Laravel Boilerplate ------------------------




## Instalación

La aplicación requiere de Docker y docker-compose para poder ser ejecutada.

Por defecto la aplicación corre en el puerto 80, por lo que sera necesario tenerlo disponible o bien editar el archivo docker-compose.yml y editar el puerto.

ports:
      - "XX:80"


```sh
git clone git@gitlab.com:gmancerah/backend-coding-challenge.git
cd backend-coding-challenge
cp .env.example .env
docker-compose up -d
docker exec parrot-rest-app composer install
docker exec parrot-rest-app php artisan key:generate
docker exec parrot-rest-app php artisan migrate
docker exec parrot-rest-app php artisan db:seed
docker exec parrot-rest-app php artisan passport:install
docker exec parrot-rest-app npm install
docker exec parrot-rest-app npm run dev
```


## Rutas / Endpoints

La siguiente tabla describe brevemente los endpoints generados para esta prueba. 

| Ruta | Descripción |
| ------ | ------ |
| (POST) /api/auth/login | Este es el metodo que se utiliza para identificarse y generar un token mediante OAuth2( Bearer) con un tiempo de expiracion de 1 día. |
| (POST) /api/auth/signup | Esta ruta nos permite crear usuarios, se requiere de permisos de administrador para generar usuarios. |
| (GET) /api/auth/logout | Permite finalizar la sesión |
|(GET) /api/auth/me | Obtiene la información del usuario |
| (POST) /api/v1/orders | Permite ingresar ordenes al sistema |
| (GET) /api/v1/orders | Permite visualizar las ordenes generadas por el usuario |
| (GET) /api/v1/orders/date/{from_date?}/{to_date?} | Permite visualizar las ordenes creadas en un periodo de tiempo, en caso de no incluirse el periodo se entregaran las ordenes generadas en el día actual  |
| (GET) /api/v1/orders/users/{user_id?} | Los administradores pueden visualizar las ordenes de otros usuarios indicando el id del usuario, en caso de no especificarse se mostrará la informacion del propio usuario.  |
| (GET) /api/v1/orders/{invoice_id?} | Los usuarios podran consultar los detalles de la orden que se especifique por parametro, en caso de no indicar un numero de orden se mostrara la ultima orden registrada.  |
| (GET) /api/v1/report/date/{from_date?}/{to_date?}/{store_id?} | En este enpoint se puede generar un reporte de los productos mas vendidos durante un periodo en especifico. El parametro de tienda es opcional y resulta util para que los administradores puedan revisar otras tiendas. Con el fin de evitar que los usuarios mezclaran productos o clientes, se creo una estructura que define tiendas y negocios. El proposito es poder crecer el sistema mas adelante, se dejaron todas las bases necesarias pero se dejo como una siguiente fase.    |


## Como generar credenciales

Para poder generar un token es necesario hacer un llamado (POST) a /api/auth/login y enviar las siguientes credenciales en el body.

{
  "email": "admin@admin.com",
  "password": "secret",
  "remember_me": true
}

Esta ruta nos devolverá un token con el que podremos hacer las peticiones a nuestros endpoints.
```sh
Ej: eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiOWY2N2ZlZTVkNDIwNWJmZWFjNDlhMjg5YmRmN2E3NzRiY2NkODg1MzA3NGM5ZThkZmIwZjlmNmQzNTcwZjZhMTlmNjE2M2JjYWVlNTU1YjYiLCJpYXQiOjE2MjgwMTQxOTMuMzMyMjczLCJuYmYiOjE2MjgwMTQxOTMuMzMyMzM5LCJleHAiOjE2NTk1NTAxOTMuMjgyOTk5LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.o9GKD6e1zlidY9lVmOK7LZLhz9Q77t4QE3Rj8kXslDD5GLMDbq48IBFepNDb9JCsY5GaGJez-zJKCmR9l-iflvg--vb9CS-_caER5LFV-cOHJ1gqJOEKkbsk8I_eS1mpYm8l30xowlZSEql02D1EpYAf-ytUrszn-l2kg8cuE9EK99HdyJ84UandeBziGqnZz6WmARosIU3sHo49XDuv8_dzrWY5Gqf7ovV9j6RK58_SV8g3EHljhRMvEGTZ-VbNzjoJTLf--x8lrKV0YntAdpRWeuCSxLpl4uFHONaITFzLjpw6ic_FegFSoAQ0ua2ItcvYlV8gYT8iPTkhevb9mPXhOCUkMMewv8eoN3gBNd4ZkpDQ2S9Q0knJH_TJ7rHuIgUAD0QUNQt-iroLVbNOzYDzlQnwxk7lcslO4z89Ywdxybya5fOC4K_PEqywhDT8-a64CcQ1749mJo9y5MQYwS_v0PsRpRJhHwYezpaORoObsmIu120DT6VUsNq7jbkTA8yk6CvcOOr8USyiEVIy2iGRoiJhRmQuaNzMzUjf53nwKvcKm5Dwt0MwNzZAc2NJycl-zHVTT1p3DxZwwSs-IOFcdB8PbcifoxAPfoqjsIP1mpLsrBkJnHn4EtCyxPohP2STnb2lpuPRCPPcx58IOSb7ueGDV-aNxq7ar8cROS4
```

Con este token es posible hacer todas las pruebas pues el usuario cuenta con permisos de administrador. En caso de requerir otro usuario de pruebas existen 4 usuarios definidos en las migraciones user[0-3]@user.com, con la misma contraseña que el admin.


## Como ingresar ordenes

Se debe hacer una petición (POST) a /api/v1/orders y enviar un json en el body con la siguiente estructura:

{   "client":"Casa de las Flores",
  "total": 314.00,
  "product_detail":[
    {"name":"Producto 1",
    "price":50.00},
    {"name":"Producto 2",
    "price":40.00},
    {"name":"Producto 3",
    "price":10.00},
    {"name":"Producto 4",
    "price":20.00},
    {"name":"Producto 5",
    "price":22.00},
    {"name":"Producto 6",
    "price":23.00},
    {"name":"Producto 7",
    "price":34.00},
    {"name":"Producto 8",
    "price":10.00},
    {"name":"Producto 9",
    "price":15.00}
  ]
}

