<?php

namespace App\Http\Controllers\Frontend;
//require_once 'vendor/faker/src/autoload.php';
use App\Models\Business;

/**
 * Class HomeController.
 */
class HomeController
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {

       return view('frontend.index');
    }
}
