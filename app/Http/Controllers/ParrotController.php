<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Str;
use Illuminate\Support\Facades\Auth;
use App\Domains\Auth\Models\User;
use App\Models\Store;
use App\Models\Business;
use App\Models\Product;
use App\Models\Invoice;
use App\Models\Client;
use App\Models\InvoiceDetails;
use App\Http\Resources\InvoiceCollection;
use Carbon\Carbon;
// use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\DB;

class ParrotController extends Controller
{
    
    /**
     * Registro de usuario
     */
    public function newInvoice(Request $request)
    {
        $invoice_json=$request->json()->all();
        $information=$this->getStore();
        $client_id=$this->clientExist($invoice_json['client'],$information["business_id"]);
        $invoice_number=$this->createInvoice($information["store_id"],$client_id,Auth::id(),$invoice_json['total']);

        if(isset($invoice_json['product_detail'])){
            $totalWidth =   $this->addProducts($invoice_json['product_detail'],$information,$invoice_number->id);
        }else{
            return $this->ErrorFormat();
        }
            
    }

    public function createInvoice($store_id,$client_id,$user_id,$total,$date=null){
        $invoice = new Invoice;
        $invoice->store_id=$store_id;
            $invoice->client_id=$client_id;
            $invoice->user_id=$user_id;
            $invoice->total=$total;
            if(!is_null($date)){
                $invoice->created_at = $date; 
                $invoice->updated_at = $date;
            }
            $invoice->save();
            return $invoice;
    }

    protected function ErrorFormat(){
        return response()->json([
                    'message' => 'Unprocessable Entity'
                ], 422);
    }

    /**
     * Encuentro la tienda o creo una y su negocio
     */
    protected function getStore($store_information=null,$business_information=null){
        $store_id=DB::table("stores_users")->where("user_id",Auth::id())->get()->first();
        if ($store_id === null){
            $business_data  = $this->createBusiness($business_information);
            $store_data     = $this->createStore($store_information);
            $store_data->users()->attach(Auth::id());
            $business_data->stores()->attach($store_data->id);
            return array("store_id"=>$store_data->id,"business_id"=>$business_data->id);
        }else{
            $business_id=Store::find($store_id->id)->businesses()->first();
            return array("store_id"=>$store_id->id,"business_id"=>$business_id->id);
        }
    }

    /**
     * Crea un negocio
     */
     public function createBusiness($business_information=null)
    {
        $business = new Business;
        if($business_information!=null){
            $business->name = $business_information["name"];
            $business->logo = $business_information["logo"];
            $business->address = $business_information["address"];
            $business->city = $business_information["city"];
            $business->state = $business_information["state"];
            $business->country = $business_information["country"];
            $business->zip = $business_information["zip"];
            $business->phone = $business_information["phone"];
            $business->latitude = $business_information["latitude"];
            $business->longitude = $business_information["longitude"];
        }else{
            $business->name = "Business ".Auth::id();
        }
        $business->save();
        return $business;
    }

    /**
     * Crea una tienda
     */
    public function createStore($store_information)
    {
        $store = new Store;
        if($store_information!=null){
            $store->name = $store_information["name"];
            $store->photo = $store_information["photo"];
            $store->address = $store_information["address"];
            $store->city = $store_information["city"];
            $store->state = $store_information["state"];
            $store->country = $store_information["country"];
            $store->zip = $store_information["zip"];
            $store->phone = $store_information["phone"];
            $store->latitude = $store_information["latitude"];
            $store->longitude = $store_information["longitude"];
        }else{
            $store->name = "Store 1";
        }
        $store->save();
        return $store;
    }


    /**
     * Agregamos productos
     */
    public function addProducts($products,$information,$invoice_id,$date=null){
        $total=0;
        foreach($products as $product){
            if (isset($product["price"]) && is_numeric($product["price"])){
                try {
                    $product_name =$this->cleanProductName($product["name"]);
                    $product_id = $this->ProductExist($product_name,$information["business_id"]);
                     $invoice_details = new InvoiceDetails;
                     $invoice_details->invoice_id = $invoice_id;
                     $invoice_details->product_id = $product_id;
                     $invoice_details->price = $product["price"];
                     if(!is_null($date)){
                        $invoice_details->created_at = $date; 
                        $invoice_details->updated_at = $date;
                     }

                     $invoice_details->save();
                    
                    
                    $total=$total+floatval($product["price"]);
                } catch (Exception $e) {
                    
                }  
            }
            
            

        }

        return $total;
        
    }

    /**
     * Limpio el nombre del producto. Se separa en una funcion para extenderla
     */
    protected function cleanProductName($product){
        $product=trim($product);
        return $product;
    }


    protected function createProduct($product_name,$business_id,$extras=null){
        $product= new Product;
        $product->business_id=$business_id;
        $product->name=$product_name;
        $product->slug=Str::slug($product_name,"-");
        if($extras!=null){
            $product->image=$extras["image"];
            $product->category=$extras["category"];
            $product->description=$extras["description"];    
        }
        $product->save();
        return $product;
    }


    public function createClient($client_name,$business_id){
        $client= new Product;
        $client->business_id=$business_id;
        $client->name=$client_name;
        $client->slug=Str::slug($client_name,"-");
        $client->save();
        return $client;
    }

    protected function ProductExist($product_name,$business_id){
         $product=Product::where('slug',Str::slug($product_name,"-"))->where('business_id',$business_id)->first();
         if ($product === null){
            $product_id=$this->createProduct($product_name,$business_id);
            return $product_id->id;
        }else{
            return $product->id;
        }


    }

     public function clientExist($client_name,$business_id){
         $client=Client::where('slug',Str::slug($client_name,"-"))->where('business_id',$business_id)->first();
         if ($client === null){
            $client_id=$this->createClient($client_name,$business_id);
            return $client_id->id;
        }else{
            return $client->id;
        }


    }


    
    public function getDatedInvoices($from_date = null, $to_date=null){
        if (is_null($from_date)){
            $from_date=Carbon::today();
        }
        if(is_null($to_date)){
            $to_date=Carbon::now();
        }
        $data = Invoice::whereBetween('created_at',[$from_date,$to_date])->paginate(); 
        return response()->json($data, 200);

    }

     public function readInvoices($user_id = null){

        $data = Invoice::where('user_id',Auth::id())->paginate(); 

        return response()->json($data, 200);

    }

     public function readInvoice($invoice_id = null){
        
        if(is_null($invoice_id)){

            if(Auth::user()->isAdmin()){

                $data = Invoice::orderBy('created_at', 'desc')->first();
            }else{

                $data = Invoice::where('user_id',Auth::id())->orderBy('id', 'desc')->first();
            }
        }else{

            if(Auth::user()->isAdmin()){

                $data = Invoice::where('id',$invoice_id)->get();
                
            }else{

                $data = Invoice::where('user_id',Auth::id())->where('id',$invoice_id);
            }

        }
       $data->put("products",DB::table('invoice_details')->where('invoice_id',$invoice_id)->join('products', 'invoice_details.product_id', '=', 'products.id')->get());

        // dd(DB::table('invoice_details')->where('invoice_id',$invoice_id)->get());
        return response()->json($data, 200);
    }

    public function readUserInvoices($user_id = null){
        if(Auth::user()->isAdmin()){
            if(!is_null($user_id)){
                $data = Invoice::where('user_id',$user_id)->paginate();
            }else{
                $data = Invoice::paginate();
            }
            return response()->json($data, 200);
        }else{
            $data = Invoice::where('user_id',Auth::id())->paginate();
        }
        
        return response()->json($data, 200);

    }
    protected function getDefaultStore(){
        $store_query=DB::table("stores_users")->where("user_id",2)->get()->first();
        if ($store_query === null){

        }
        return $store_query;
    }

    public function ReportDated($from_date = null, $to_date=null,$store_id=null){
        
        if (is_null($from_date)){
            $from_date=Carbon::yesterday();
        }
        if(is_null($to_date)){
            $to_date=Carbon::now();
        }

        
        if(is_null($store_id)){
            $user_data=$this->getStore();
            $store_id=$user_data["store_id"];
        }else{
            if(!Auth::user()->isAdmin()){
                
                if (DB::table("stores_users")->where("user_id",Auth::id())->where("store_id",$store_id)->count() == 0){
                    return response()->json([
                    'message' => 'No access to store'
                ], 422);
                }

            }

        }
        
        $data = InvoiceDetails::selectRaw("SUM(price) as venta_total, products.name as producto, count(1) as piezas_vendidas")->whereBetween('invoice_details.created_at',[$from_date,$to_date])->groupBy('invoice_details.product_id')->join('products', 'invoice_details.product_id', '=', 'products.id')->join('invoices', 'invoice_details.invoice_id', '=', 'invoices.id')->where("invoices.store_id",$store_id)->orderBy('venta_total', 'desc')->paginate(); 
        $data->put("Store_id",$store_id);
        return response()->json($data, 200);

    }

}
