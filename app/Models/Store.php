<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Domains\Auth\Models\User;

class Store extends Model
{
    use HasFactory;

    public function businesses()
    {
   //return $this->belongsToMany(RelatedModel, pivot_table_name, foreign_key_of_current_model_in_pivot_table, foreign_key_of_other_model_in_pivot_table);
   return $this->belongsToMany(
        Store::class,
        'businesses_stores',
        'store_id',
        'business_id');
    }


    public function users()
    {
   
   return $this->belongsToMany(
        Store::class,
        'stores_users',
        'store_id',
        'user_id'
        );
    }
}
