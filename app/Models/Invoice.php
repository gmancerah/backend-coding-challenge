<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;


     public function stores()
    {
   
   return $this->belongsToMany(
        Store::class,
        'stores_users',
        'store_id',
        'user_id'
        );
    }


     public function clients()
    {
   
   return $this->belongsToMany(
        Client::class,
        'stores_users',
        'store_id',
        'user_id'
        );
    }

    


     public function users()
    {
   
   return $this->belongsToMany(
        Users::class,
        'stores_users',
        'store_id',
        'user_id'
        );
    }
}
