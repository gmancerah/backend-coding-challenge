<?php

use Illuminate\Http\Request;
use \App\Http\Controllers\APIAuthController;
use \App\Http\Controllers\ParrotController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', [APIAuthController::class, 'login']);
    Route::post('signup', [APIAuthController::class, 'signUp']);


    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        
        Route::get('logout', [APIAuthController::class, 'logout']);
        Route::get('me', [APIAuthController::class, 'user']);
        Route::get('users', [APIAuthController::class, 'users']);
    });
});


Route::group([
    'prefix' => 'v1'
], function () { 
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::post('orders', [ParrotController::class, 'NewInvoice']);
        Route::get('orders', [ParrotController::class, 'readInvoices']);
        Route::get('orders/date/{from_date?}/{to_date?}', [ParrotController::class, 'getDatedInvoices'])->where(['from_date' => '[0-9]{4}-[0-9]{2}-[0-9]{2}']);
        Route::get('orders/users/{user_id?}', [ParrotController::class, 'readUserInvoices'])->whereNumber('page');    
        Route::get('order/{invoice_id?}', [ParrotController::class, 'readInvoice'])->whereNumber('invoice_id');



        Route::get('report/date/{from_date?}/{to_date?}/{store_id?}', [ParrotController::class, 'ReportDated'])->where(['from_date' => '[0-9]{4}-[0-9]{2}-[0-9]{2}','to_date'=>'[0-9]{4}-[0-9]{2}-[0-9]{2}'])->whereNumber('store_id');
    });
});

Route::fallback(function(){
    return response()->json([
        'message' => 'Page Not Found.'], 404);
});