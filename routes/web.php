<?php

use App\Http\Controllers\LocaleController;


/*
 * Global Routes
 *
 * Routes that are used between both frontend and backend.
 */

// Switch between the included languages
Route::get('lang/{lang}', [LocaleController::class, 'change'])->name('locale.change');


// Route::get('/greeting', function () {
    
//     for ($i = 0; $i < rand(1, 3); $i++) {
//                     $faker_factory = Faker\Provider\en_US\Company();
//                     $faker_image = Faker\Provider\Image::create();
//                     $faker_address = Faker\Provider\en_US\Address();
//                     $faker_phone = Faker\Provider\en_US\PhoneNumber();

                    
//                     $test=[
//                         'name' => $faker_factory->company,
//                         'logo' => $faker_image->imageUrl($width = 640, $height = 480,'abstract'),
//                         'address' => $faker_address->address,
//                         'city' => $faker_address->city,
//                         'state' => $faker_address->state,
//                         'country' => $faker_address->country,
//                         'zip' => $faker_address->postcode,
//                         'phone' => $faker_phone->phoneNumber,
//                         'latitude' => $faker_address->latitude(),
//                         'longitude' => $faker_address->longitude()

//                     ];
//                     print_r($test);
//     }
//     return 'Hello World';
// });


/*
 * Frontend Routes
 */
Route::group(['as' => 'frontend.'], function () {
    includeRouteFiles(__DIR__.'/frontend/');
});

/*
 * Backend Routes
 *
 * These routes can only be accessed by users with type `admin`
 */
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    includeRouteFiles(__DIR__.'/backend/');
});
